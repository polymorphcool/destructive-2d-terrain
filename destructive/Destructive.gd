extends Node

export(Texture) var texture_block = null
export(Texture) var texture_ghost = null

var block_size = 40
var block_margin = 5

var screensize
var pieces = []

func _ready():
	screensize = get_viewport().get_visible_rect().size
	
	var centerx = screensize.x / 2 - 200
	var centery = screensize.y / 2 + 150
	
	# generation of a large quantity of blocks
	for y in range( 0, 4 ):
		for x in range( 0, 30 ):
			var p = Vector2( centerx + x * block_size, centery + y * block_size )
			add_block( p )

func add_block( pos ):
	
	# generation geometry
	var poly = PoolVector2Array()
	poly.append( Vector2( -block_size*0.5, -block_size*0.5 ) )
	poly.append( Vector2( block_size*0.5, -block_size*0.5 ) )
	poly.append( Vector2( block_size*0.5, block_size*0.5 ) )
	poly.append( Vector2( -block_size*0.5, block_size*0.5 ) )
	
	# generation texture coordinates
	var uvs = PoolVector2Array()
	uvs.append( Vector2( 0,0 ) )
	uvs.append( Vector2( texture_block.get_width(),0 ) )
	uvs.append( Vector2( texture_block.get_width(),texture_block.get_height() ) )
	uvs.append( Vector2( 0,texture_block.get_height() ) )
	
	# generation of displayable object
	var block = Polygon2D.new()
	block.polygon = poly
	block.uv = uvs
	block.position = pos
	block.texture = texture_block
	# random color
	block.color = Color( rand_range(0,1), rand_range(0,1), rand_range(0,1) )
	$blocks.add_child(block)
	
	# generation of collision object
	var shape = CollisionPolygon2D.new()
	shape.polygon = poly
	shape.position = pos
	$StaticBody2D.add_child(shape)
	
	# generation of collision detection object (bigger)
	poly = PoolVector2Array()
	var halfsize = block_size * 0.5 + block_margin
	poly.append( Vector2( -halfsize, -halfsize ) )
	poly.append( Vector2( halfsize, -halfsize ) )
	poly.append( Vector2( halfsize, halfsize ) )
	poly.append( Vector2( -halfsize, halfsize ) )
	shape = CollisionPolygon2D.new()
	shape.polygon = poly
	shape.position = pos
	$Area2D.add_child(shape)

func _process(delta):
	pass

func _on_Area2D_body_shape_entered( body_id, body, body_shape, area_shape ):
#	print( body_id, ' / ', body, ' / ', body_shape, ' / ', area_shape )
	
	# moving displayable object from blocks to ghost
	var b = $blocks.get_child( area_shape )
	$blocks.remove_child( b )
	$ghost.add_child( b )
	b.set_owner( $ghost )
	b.texture = texture_ghost
	
	# destruction of collision objects
	$StaticBody2D.remove_child( $StaticBody2D.get_child( area_shape ) )
	$Area2D.remove_child( $Area2D.get_child( area_shape ) )
